﻿using System;
using System.Collections.Generic;

namespace les_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Player Player = new Player();
            Player.ShowInfo(100, 100, 50);
            Player.SelektWeapon();

        }
        class Player
        {
            protected Weapon weapon1 = new Pistol();
            protected Weapon weapon2 = new Shotgun();
            protected Weapon weapon3 = new Machine();
            protected Weapon weapon4 = new Rifle();

            protected int Health;
            protected int Armor;
            protected int PlayerSpeed;

            

            public virtual void ShowInfo(int Health, int Armor, int PlayerSpeed)
            {
                this.Health = Health;
                this.Armor = Armor;
                this.PlayerSpeed = PlayerSpeed;

                Console.WriteLine("Здоровье",+ Health);
                Console.WriteLine("Броня",+ Armor);
                
            }

            public void SelektWeapon()
            {
                Console.WriteLine("*************");
                Console.WriteLine("Выбрать оружие: \n 1. Pistol \n 2. Shotgun \n 3. Machine \n 4. Rifle");
                int Select = Convert.ToInt32(Console.ReadLine());

                if (Select == 1)
                {
                    weapon1.ShowWeapon();
                }
                if (Select == 2)
                {
                    weapon2.ShowWeapon();
                }
                if (Select == 3)
                {
                    weapon3.ShowWeapon();
                }
                if (Select == 4)
                {
                    weapon4.ShowWeapon();
                }
            }
            

        }

        class Weapon
        {
            protected int Damage;
            protected int BulletsNumber;
            protected int ShotRange;
            protected int GunWeight;

            public void SetDamage (int Damage)
            {
                this.Damage = Damage;
            }
            public void SetBulletsNumber (int BulletsNumber)
            {
                this.BulletsNumber = BulletsNumber;
            }
            public void SetShotRange (int ShotRange)
            {
                this.ShotRange = ShotRange;
            }
            public void SetGunWeight (int GunWeight)
            {
                this.GunWeight = GunWeight;
            }

            public virtual void ShowWeapon()
            {
                Console.WriteLine("SelektWeapon");
            }


        }

        class Pistol : Weapon
        {
            private string pistol;
            public Pistol()
            {
                pistol = "Пистолет";
                Damage = 10;
                BulletsNumber = 8;
                ShotRange = 40;
                GunWeight = 1;

            }

            public override void ShowWeapon()
            {
                base.ShowWeapon();
                Console.WriteLine(pistol);
                Console.WriteLine("Урон:" + Damage);
                Console.WriteLine("пуль в магазине:" + BulletsNumber);
                Console.WriteLine("Дальность стрельбы:" + ShotRange);
                Console.WriteLine("Вес оружия:" + GunWeight);
            }
        }

        class Shotgun : Weapon
        {
            private string shotgun;
            public Shotgun()
            {
                shotgun = "Дробовик";
                Damage = 50;
                BulletsNumber = 6;
                ShotRange = 80;
                GunWeight = 6;
            }

            public override void ShowWeapon()
            {
                base.ShowWeapon();
                Console.WriteLine(shotgun);
                Console.WriteLine("Урон:" + Damage);
                Console.WriteLine("пуль в магазине:" + BulletsNumber);
                Console.WriteLine("Дальность стрельбы:" + ShotRange);
                Console.WriteLine("Вес оружия:" + GunWeight);
            }
        }

        class Machine : Weapon
        {
            private string machine;
            public Machine()
            {
                machine = "Автомат";
                Damage = 30;
                BulletsNumber = 30;
                ShotRange = 250;
                GunWeight = 3;
            }

            public override void ShowWeapon()
            {
                base.ShowWeapon();
                Console.WriteLine(machine);
                Console.WriteLine("Урон:" + Damage);
                Console.WriteLine("пуль в магазине:" + BulletsNumber);
                Console.WriteLine("Дальность стрельбы:" + ShotRange);
                Console.WriteLine("Вес оружия:" + GunWeight);
            }
        }

        class Rifle : Weapon
        {
            private string rifle;
            public Rifle()
            {
                rifle = "Винтовка";
                Damage = 90;
                BulletsNumber = 5;
                ShotRange = 750;
                GunWeight = 5;
            }
            public override void ShowWeapon()
            {
                base.ShowWeapon();
                Console.WriteLine(rifle);
                Console.WriteLine("Урон:" + Damage);
                Console.WriteLine("пуль в магазине:" + BulletsNumber);
                Console.WriteLine("Дальность стрельбы:" + ShotRange);
                Console.WriteLine("Вес оружия:" + GunWeight);
            }
        }
      

    }

}